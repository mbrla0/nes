/* rp2a03.h -- Ricoh 2A03 */
#ifndef __RP2A03_H__
#define __RP2A03_H__

#include <stdio.h>	/* For fprintf()		*/
#include <string.h>	/* For memcpy()			*/
#include "mmap.h"	/* For mapped memory	*/

/* CPU status flag bit masks. See the status
 * register in the rp2a03 structure for more
 * information on these. */
#define FLAG_CARRY      0x01
#define FLAG_ZERO       0x02
#define FLAG_INTINHIBIT 0x04
#define FLAG_BCD        0x08
#define FLAG_BREAKPOINT 0x10
#define FLAG_OVERFLOW   0x40
#define FLAG_NEGATIVE   0x80

struct rp2a03{
	char a; /* Accumulator */
	char x; /* X Register */
	char y; /* Y Register */

	/* 16-bit stack pointer.
	 * The higher 8 bits are hard-coded and must always
	 * point to the first memory page, while the lower
	 * 8 bits are freely modifiable.
	 */
	union {
		unsigned short x;
		struct{
			unsigned char h;
			unsigned char l;
		} __attribute__((packed)) i8;
	} sp;

	/* Little bit of a cheat, when the stack pointer is
	 * set, its new value is cloned into a base stack
	 * pointer, allowing for quick underflow testing.
	 */
	union {
		unsigned short x;
		struct{
			unsigned char h;
			unsigned char l;
		} __attribute__((packed)) i8;
	} bsp;

	/* Program counter */
	unsigned short pc;

	/* 8-bit Status Register.
	 * Encodes seven single-bit flags at bit offsets:
	 * 		0x01: Carry
	 * 		0x02: Zero flag
	 * 		0x04: Interrupt inhibit
	 * 		0x08: Bynary-Coded Decimal (inactive)
	 * 		0x10: Breakpoint
	 * 		0x20: -- Blank --
	 * 		0x40: Overflow
	 * 		0x80: Negative/Sign
	 */
	char sr;
};

/* rp2a03_pop() -- Pops length bytes from the stack
 * and writes them to the target returning how many
 * bytes were popped or a negative error code. */
int
rp2a03_pop(
		struct rp2a03* cpu,
		struct memmap* map,
		unsigned char length,
		void *target);

/* rp2a03_push() -- Reads length bytes from source and
 * pushes them atop the stack returning how many bytes
 * were pushed or a negative error code. */
int
rp2a03_push(
		struct rp2a03* cpu,
		struct memmap* map,
		unsigned char length,
		void *source);

/* rp2a03_run() -- Runs the CPU for at most the given
 * number of cycles, stopping at the first operation
 * to take longer than the remaining cycles.
 *
 * Parameters:
 * 		- cpu: The CPU structure
 * 		- map: The memory map
 * 		- cycles: How many cycles to run for
 * 
 * Returns: The remaining number of cycles, if any.
 */
unsigned int
rp2a03_run(
		struct rp2a03* cpu,
		struct memmap* map,
		unsigned int cycles);

#endif
