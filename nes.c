#include "ines.h"
#include <stdio.h>

int
main(int argc, char **argv)
{
	if(argc < 2)
	{
		fprintf(stderr, "Usage: %s <ROM>\n", argv[0]);
		return 1;
	}

	char *filename;
	filename = argv[argc - 1];

	struct ines ines;
	if(ines_open(filename, &ines) != 0)
		return 1;

	/* Print ROM details */
	printf(
			"Details for ROM at: %s\n"
			"Type: %s\n"
			"Mapper: %d\n"
			"Submapper: %d\n"
			"PGR ROM: %d bytes\n"
			"CHR ROM: %d bytes\n"
			"VRAM mirroring mode: %d\n"
			"Four screen VRAM? %d\n"
			"Has trainer? %d\n"
			"Has persistent SRAM? %d\n"
			"Playchoice 10 game? %d\n"
			"Vs. Unisystem game? %d\n",
			filename,
			(ines.header.flags7 & INES_F7_NES20) == INES_F7_NES20_Y ? "NES 2.0" : "iNES",
			ines_mapper(&ines).mapper,
			ines_mapper(&ines).submapper,
			ines.header.pgr_rom * 16384,
			ines.header.chr_rom * 8192,
			(ines.header.flags6 & INES_F6_MIRRORING) != 0,	
			(ines.header.flags6 & INES_F6_FOURSCREEN) != 0,
			(ines.header.flags6 & INES_F6_TRAINER) != 0,
			(ines.header.flags6 & INES_F6_PERSISTENT) != 0,
			(ines.header.flags7 & INES_F7_PLAYCHOICE) != 0,
			(ines.header.flags7 & INES_F7_UNISYSTEM) != 0);

	return 0;
}
