#include "ines.h"

int
ines_open(const char* path, struct ines *ines)
{
	ines->f = fopen(path, "rb");
	if(ines->f == NULL){
		perror("[ines_open()] Could not open file at %d");
		return -1;
	}

	/* Load the iNES header */
	size_t read = fread(&ines->header, sizeof(struct ines_header), 1, ines->f);
	if(read < 1){
		perror("[ines_open()] Could not read iNES header");
		return -1;
	}

	return 0;
}

struct ines_mapper
ines_mapper(struct ines *ines)
{
	/* Load the lower eight bits in the original spec */
	struct ines_mapper mapper;
	mapper.mapper = 0;
	mapper.mapper |= (ines->header.flags6 & INES_F6_MAPPER_LO) >> 4;
	mapper.mapper |= (ines->header.flags7 & INES_F7_MAPPER_HI) >> 0;

	/* If this this an NES 2.0 header, load the extra 4 bits
	 * along with the submapper */
	if((ines->header.flags7 & INES_F7_NES20) == INES_F7_NES20_Y){
		mapper.mapper    |= (ines->header.flags8 & INES_F8_MAPPER_EX) << 4;
		mapper.submapper  = (ines->header.flags8 & INES_F8_SUBMAPPER);
	}else
		/* This is an iNES ROM and submappers are not available */
		mapper.submapper = -1;

	return mapper;
}
