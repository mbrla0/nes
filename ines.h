/* ines.h -- Support for the iNES ROM format 
 * along with its NES 2.0 extetion. */
#ifndef __INES_H__
#define __INES_H__

#include <stdio.h>

/* iNES-compatible definitions for flags 6 and 7 */
#define INES_F6_MIRRORING	0x01	/* 1: Vertical, 0: Horizontal 		*/
#define INES_F6_PERSISTENT	0x02	/* Persistent SRAM at 6000-7FFFh?	*/ 
#define INES_F6_TRAINER		0x04	/* 512-bit trainer at 7000h?		*/
#define INES_F6_FOURSCREEN	0x08	/* Four screen VRAM mode 			*/
#define INES_F6_MAPPER_LO	0xf0	/* Lower four bits of the mapper 	*/

#define INES_F7_UNISYSTEM	0x01	/* Is this a Vs. Unisystem game?	*/
#define INES_F7_PLAYCHOICE	0x02	/* Is this a Playchoice-10 game?	*/
#define INES_F7_NES20		0x0c	/* Is this a NES 2.0 file? 			*/
#define INES_F7_NES20_Y		0x04	/* Is this a NES 2.0 file? Yes.		*/
#define INES_F7_MAPPER_HI	0xf0	/* Higher four bits of the mapper	*/

#define INES_F9_VIDEOSYS	0x01	/* Rarely used video mode bit */

/* Extended NES 2.0 definitions using flags 8-13 */
#define INES_F8_MAPPER_EX	0xf0	/* Extra four bits of the mapper	*/
#define INES_F8_SUBMAPPER	0x0f	/* Four bit long unsigned submapper	*/

struct ines_header{
	/* Constant label at the beggining of the
	 * header identifying this as an iNES ROM
	 * file. Must always be [4Eh,45h,53h,1Ah],
	 * or "NES<EOF>".
	 */
	char label[4];

	/* Size of the PRG ROM in 16KB units */
	unsigned char pgr_rom;

	/* Size of the CHR ROM in 8KB units */
	unsigned char chr_rom;

	/* Flags 6 and 7: Bitfields used in
	 * the original iNES spec for storing
	 * assorted ROM bits */
	char flags6;
	char flags7;

	/* This one's meaning depends on the version
	 * of the specification we're using:
	 * 		iNES	- Size of PRG RAM in 8KB units
	 * 		NES 2.0	- The flags 8 bitfield 
	 */
	char flags8;

	/* Flags 9-15: Bitfields unused in the
	 * original iNES spec, but used in the NES
	 * 2.0 extention to store further ROM
	 * information. (While, technically, the
	 * original spec did use the LSB of flags9
	 * to store the video system, it's not
	 * usual for ROMs to ever use that bit.)
	 */
	char flags9,  flags10, flags11;
	char flags12, flags13, flags15;
};

struct ines_mapper{
	/* Base mapper number, being limited to its
	 * lower eight bits in iNES ROMs, while for
	 * NES 2.0, the first twelve bytes may be
	 * used. No special behaviour is required
	 * to test for either ROM format. */
	short mapper;

	/* Submapper, used to distinguish similar
	 * kinds of mapper between one another without
	 * requiring a new mapper number to be assigned.
	 * Only available in NES 2.0 ROMs, always
	 * having a value of -1 in iNES ROMs, a value
	 * which is unreachable in the spec's four
	 * unsigned bit range. Mappers implementations
	 * should always check for a value of -1 before
	 * determining their own behaviour. */
	char submapper;
};

struct ines{
	/* File handle for the ROM */
	FILE *f;
	/* Header */
	struct ines_header header;
};

/* ines_open() -- Open and validade an iNES or
 * NES 2.0 compatible ROM file
 *
 * Returns:
 * 		0: Sucess
 * 	*/
int
ines_open(const char* path, struct ines *ines);

/* ines_mapper() -- Returns the mapper number
 * associated with this ROM file */
struct ines_mapper
ines_mapper(struct ines *ines);

#endif
