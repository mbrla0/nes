/* mappers.h -- iNES-style NES mappers */
#ifndef __MAPPERS_H__
#define __MAPPERS_H__

/* Mappers rely heavily on thread-local storage
 * to function properly, while C11 provides those
 * natively, make sure this program can at least
 * compile without the standard using compiler
 * extentions, or even allowing them to be
 * disabled entirely. */
#ifndef _THREAD_LOCAL
	#if __STDC_VERSION__ >= 201112L && !__STDC_NO_THREADS__
		/* We've got native thread support in C11 */
		#define _THREAD_LOCAL _Thread_local
	#elif __GNUC__ > 3
		/* GCC has supported the __thread since version 3.3.1 */
		#define _THREAD_LOCAL __thread
	#elif __llvm__ >= 2
			/* As has LLVM as of version 2.0 */
			#define _THREAD_LOCAL __thread
	#else
			#error "Could determine thread-local storage capabilites. Please enable C11, use either GCC or Clang, or define _THREAD_LOCAL to either the thread-local storage directive of your compiler or the empty string to disable thread-local storage entirely."
	#endif
#endif

#include "mmap.h"

/*void* mapper_0000_0(struct memmap*, unsigned short);
void* mapper_0001_0(struct memmap*, unsigned short);
void* mapper_0002_0(struct memmap*, unsigned short);
void* mapper_0003_0(struct memmap*, unsigned short);
void* mapper_0004_0(struct memmap*, unsigned short);
void* mapper_0005_0(struct memmap*, unsigned short);
void* mapper_0006_0(struct memmap*, unsigned short);
void* mapper_0007_0(struct memmap*, unsigned short);
void* mapper_0008_0(struct memmap*, unsigned short);
void* mapper_0009_0(struct memmap*, unsigned short);
void* mapper_0010_0(struct memmap*, unsigned short);
void* mapper_0011_0(struct memmap*, unsigned short);
void* mapper_0012_0(struct memmap*, unsigned short);
void* mapper_0013_0(struct memmap*, unsigned short);
void* mapper_0014_0(struct memmap*, unsigned short);
void* mapper_0015_0(struct memmap*, unsigned short);


#define s(m) m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m
static mmap_mapper _MAPPERS_FNMAP[]= {
	s(mapper_0000_0), s(mapper_0001_0), s(mapper_0002_0), s(mapper_0003_0),
	s(mapper_0004_0), s(mapper_0005_0), s(mapper_0006_0), s(mapper_0007_0),
	s(mapper_0008_0), s(mapper_0009_0), s(mapper_0010_0), s(mapper_0011_0),
	s(mapper_0012_0), s(mapper_0013_0), s(mapper_0014_0), s(mapper_0015_0),
};
#undef s
*/
mmap_mapper
mappers_resolve(unsigned short mapper, char submapper);

#endif
