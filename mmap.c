#include "mmap.h"

void*
mmap_resolve(
		struct memmap *map,
		unsigned short addr)
{
	/* Check for the vector table and then check for
	 * the nontrivial mapping range */
	if(addr > 0xfff9){
		/* Vector table */
		return &((char*) map->vector_table)[addr - 0xfffa];
	}else if(addr > 0x401f){
		/* Call the mapper function */
		return map->mapper(map, addr);
	}

	/* Trivial mapping */
	if(addr < 0x2000){
		/* RAM (mirrored at multiples of 0x800) */
		return &((char*) map->ram)[addr % 0x800];
	}else if(addr < 0x4000){
		/* PPU registers (mirrored at multiples of 0x08) */
		return &((char*) map->ppu_reg)[(addr - 0x2000) % 8];
	}else if (addr < 0x4018){
		/* APU and I/O registers */
		return &((char*) map->apu_io_reg)[addr - 0x4000];
	}else{
		/* Normally disabled functions */
		return &((char*) map->debug)[addr - 0x4018];
	}
}
